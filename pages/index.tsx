import type { NextPage, GetStaticProps, InferGetStaticPropsType } from "next";

import styles from "../styles/Home.module.scss";

import Banner from "../components/Banner/banner";
import Head from "next/head";
import Image from "next/image";
import Card from "../components/card/card";
import coffeeStoresfromJson from "../data/coffee-stores.json";
import { fetchCoffeeStores } from "../lib/coffee-stores";

export const getStaticProps: GetStaticProps = async (context) => {
  const data = await fetchCoffeeStores();

  // console.log("4 square" + data);

  // console.log("props TEST 0", coffeeStoresfromJson);
  return {
    props: {
      // coffeeStores: coffeeStores,
      coffeeStores: data,
    },
  };
};

const Home = ({
  coffeeStores,
}: {
  coffeeStores: CoffeeStoresType[];
}): JSX.Element => {
  //  console.log('props TEST 1', coffeeStores);

  const handleBannerBtnOnClick: ButtonPressFunction = (): void => {
    console.log("Button clicked");
  };

  return (
    <>
      <Head>
        <meta property="description" content="Next.ts Tutorial Home" />
        <meta property="author" content="JON" />
      </Head>

      <div className={styles.container}>
        <main className={styles.main}>
          {/*

          */}
          <Banner
            buttonText="View Coffee shop nearby"
            handleOnClick={handleBannerBtnOnClick}
          />
          <div className={styles.heroImage}>
            <Image
              src="/static/imgs/hero-image.png"
              width={700}
              height={400}
              alt="hero-image"
            />
          </div>

          {coffeeStores.length > 0 && (
            <div className={styles.sectionWrapper}>
              <h2 className={styles.heading2}>Coffee Stores Delhi</h2>
              <div className={styles.cardLayout}>
                {coffeeStores.map(function (coffeeStore) {
                  return (
                    <div className={styles.card} key={coffeeStore.fsq_id}>
                      <Card
                        name={coffeeStore.name}
                        imgUrl={
                          coffeeStore.imgUrl || coffeeStoresfromJson[0].imgUrl
                        }
                        href={`/coffee-store/${coffeeStore.fsq_id}`}
                      ></Card>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </main>
      </div>
    </>
  );
};

export default Home;

/*
 

        console.log('ENV FILE TEST1 ', process.env.NEXT_PUBLIC_SITE_NAME);
  console.log('ENV FILE TEST1 ', process.env.NEXT_PUBLIC_TAG_NAME);
  console.log('ENV FILE TEST ', process.env.DB_PASSWORD);
  console.log('ENV FILE TEST_NODE ', process.env.NODE_ENV);
      <main className={[styles.main, styles.FParent].join(' ')}>
         <div className={styles.F_item_left}>
          <Hero
            buttonText={'View   Shops  nearby'}
            handleOnClick={handleBannerBtnOnClick}
          ></Hero>
        </div>

        <div className={[styles.heroImage, styles.F_item_right].join(' ')}>
          <Image src={heroimg} alt="not"></Image>
        </div>

  
*/
