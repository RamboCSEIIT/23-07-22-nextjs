import type { NextPage } from 'next';

import styles from '../styles/Home.module.scss';

import Banner from '../components/Banner/banner';
import Head from 'next/head';

const TestNext: NextPage = (): JSX.Element => {
  return (
    <>
      <Head>
        <meta property="description" content="Next.ts Tutorial TEST" />
        <meta property="author" content="RAMBO" />
      </Head>

      <div className={styles.container}>
        <main className={styles.main}>TEST</main>
      </div>
    </>
  );
};

export default TestNext;
