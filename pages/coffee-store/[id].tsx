import styles from "../../styles/coffee-store.module.scss";
import type { NextPage, GetStaticProps, InferGetStaticPropsType } from "next";
import Banner from "../../components/Banner/banner";
import Head from "next/head";
import { useRouter } from "next/router";
import Image from "next/image";
import coffeeStoresfromJson from "../../data/coffee-stores.json";
import Link from "next/link";
import { fetchCoffeeStores } from "../../lib/coffee-stores";

export const getStaticProps: GetStaticProps = async (staticProps) => {
  const params: any = staticProps.params;

  const data = await fetchCoffeeStores();

  return {
    props: {
      // coffeeStores: coffeeStores,
      coffeeStore: data.find((cof: CoffeeStoresType) => {
        //  console.log('test params', params.id);
        return cof.fsq_id.toString() === params.id;
      }),
    },
  };

  /////
  /*
  return {
    props: {
      // coffeeStores: coffeeStores,
      coffeeStore: coffeeStoresfromJson.find((cof: CoffeeStoreType) => {
        //  console.log('test params', params.id);
        return cof.id.toString() === params.id;
      }),
    },
  };
  */
};

export async function getStaticPaths() {
  const data = await fetchCoffeeStores();
  const paths = data.map((cof: CoffeeStoresType) => {
    return {
      params: {
        id: cof.fsq_id.toString(),
      },
    };
  });

  return {
    paths,
    fallback: true,
  };
}

const CoffeeStoreView = ({
  coffeeStore,
}: {
  coffeeStore: CoffeeStoresType;
}): JSX.Element => {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading ..</div>;
  }
  /////////////////

  ////////////////////////

  const handleUpvoteButton = async () => {
    console.log("Handle Up Vote");
  };

  const { name, related_places, imgUrl, location } = coffeeStore;

  //console.log(coffeeStore);
  return (
    <div className={styles.layout}>
      <Head>
        <meta property="description" content="Coffeee Store" />
        <meta property="author" content="RAMBO" />
        <title>{name}</title>
      </Head>

      <div className={styles.container}>
        <div className={styles.col1}>
          <div className={styles.backToHomeLink}>
            <Link href="/">
              <a>Back to Home</a>
            </Link>
          </div>
          <div className={styles.nameWrapper}>
            <h1 className={styles.name}>{name}</h1>
          </div>

          <Image
            src={imgUrl || coffeeStoresfromJson[0].imgUrl}
            alt={name}
            width={600}
            height={300}
            className={styles.storeImg}
          />
        </div>
        <div className={`glass ${styles.col2}`}>
          <div className="iconWrapper">
            <Image
              src="/static/icons/places.svg"
              width="24"
              height="24"
              alt="mew"
            />
            <p className={styles.text}>{location.formatted_address}</p>
          </div>
          {Object.keys(related_places).length && (
            <div className="iconWrapper">
              <Image
                src="/static/icons/nearMe.svg"
                width="24"
                height="24"
                alt="mew1"
              />
              <p className={styles.text}>{related_places.parent.name}</p>
            </div>
          )}

          <div className="iconWrapper">
            <Image
              src="/static/icons/star.svg"
              width="24"
              height="24"
              alt="mew"
            />
            <p className={styles.text}>1</p>
          </div>

          <button className={styles.upvoteButton} onClick={handleUpvoteButton}>
            Up Vote!
          </button>
        </div>
      </div>
    </div>
  );
};

export default CoffeeStoreView;
