import type { NextPage } from "next";

import styles from "../styles/Home.module.scss";

import Banner from "../../components/Banner/banner";
import Head from "next/head";
import { useRouter } from "next/router";

const CoffeeStoreIndex: NextPage = (): JSX.Element => {
  const router = useRouter();
  console.log("router", router);

  return (
    <>
      <Head>
        <meta property="description" content="Coffeee Store" />
        <meta property="author" content="RAMBO" />
      </Head>

      <div>Coffee Store Page Index</div>
    </>
  );
};

export default CoffeeStoreIndex;
