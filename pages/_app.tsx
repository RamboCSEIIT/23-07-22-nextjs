import "../styles/Gcss/normalize/_normalize.scss";
import "../styles/globals.scss";
import type { AppProps } from "next/app";

function MyApp({ Component, pageProps }: AppProps) {
  console.log("four square" + process.env.NEXT_PUBLIC_FOURSQUARE_API_KEY);

  return (
    <>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
