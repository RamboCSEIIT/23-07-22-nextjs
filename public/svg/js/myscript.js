let pixelW;
let pixelH;

var svgImg;
window.addEventListener('load', function () {
  draw();
});

window.addEventListener('resize', function (event) {
  draw();
});

function draw() {
  svgImg = document.getElementById('idSVG1');

  let dim = document.getElementsByClassName('dim')[0];

  let computedStyle = getComputedStyle(svgImg);

  pixelW = parseInt(computedStyle.width.replace('px', ''));

  pixelH = parseInt(computedStyle.height.replace('px', ''));

  let Aspect = pixelW.toFixed(2) / pixelH.toFixed(2);
  const pixelHVB = 30.0;
  const pixelWVB = pixelHVB * Aspect;
  const viewBox = '0 0 ' + pixelWVB + ' ' + pixelHVB;
  svgImg.setAttribute('width', pixelW);
  svgImg.setAttribute('height', pixelH);
  svgImg.setAttribute('viewBox', viewBox);
  dim.innerHTML =
    'View Port/View Box =>' +
    (pixelW + ':' + pixelH) +
    '/' +
    (pixelWVB.toFixed(2) + ':' + pixelHVB.toFixed(2));

  // console.log(pixelW + ':' + pixelH);
  //  console.log(pixelHVB + '*' + pixelWVB);

  // X axis so travel Y
  for (let i = 0; i < pixelH; i++) {
    // const line = document.createElement('line');

    drawAxisX(i, pixelH);
    AxisYText(i);
  }

  // Y axis so travel X
  for (let i = 0; i < pixelW; i++) {
    drawAxisY(i, pixelW);
    AxisXText(i);
  }
}

function drawAxisX(i, wh) {
  var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  line.setAttribute('x1', '0');
  line.setAttribute('y1', i.toString());
  line.setAttribute('x2', wh.toString());
  line.setAttribute('y2', i.toString());
  line.style.stroke = '#808080'; //Set stroke colour
  line.style.strokeWidth = '.02'; //Set stroke width
  svgImg.appendChild(line);
}

function drawAxisY(i, wh) {
  var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  line.setAttribute('y1', '0');
  line.setAttribute('x1', i.toString());
  line.setAttribute('y2', wh.toString());
  line.setAttribute('x2', i.toString());
  line.style.stroke = '#808080'; //Set stroke colour
  line.style.strokeWidth = '.02'; //Set stroke width
  svgImg.appendChild(line);
}
const AxisYText = (i) => {
  var labelx = document.createElementNS('http://www.w3.org/2000/svg', 'text');
  labelx.setAttribute('x', '0.1');
  labelx.setAttribute('y', i.toString());
  labelx.setAttribute('font-size', '3%');
  labelx.setAttribute('textAnchor', 'start');
  labelx.style.stroke = '#A9A9A9'; //Set stroke colour
  labelx.style.strokeWidth = '.02'; //Set stroke width

  labelx.innerHTML = i.toString();
  svgImg.appendChild(labelx);
};
const AxisXText = (i) => {
  var inc;

  if (i < 9) {
    inc = 0.4;
  } else {
    inc = 0.7;
  }

  var labelx = document.createElementNS('http://www.w3.org/2000/svg', 'text');
  labelx.setAttribute('x', (i - inc).toString());
  labelx.setAttribute('y', '0.6');
  labelx.setAttribute('font-size', '3%');
  labelx.setAttribute('textAnchor', 'start');
  labelx.style.stroke = '#A9A9A9'; //Set stroke colour
  labelx.style.strokeWidth = '.02'; //Set stroke width

  if (i == 0) {
    labelx.style.visibility = 'hidden';
  }

  labelx.innerHTML = i.toString();
  svgImg.appendChild(labelx);
};
