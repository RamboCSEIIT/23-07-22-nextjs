import { isGeneratorFunction } from 'util/types';

function AxisX(i: number) {
  return (
    <line
      x1={0}
      y1={i}
      x2={500}
      y2={i}
      key={i + 3000}
      strokeWidth={0.05}
      style={{
        stroke: 'grey',
        fill: 'none',
      }}
    />
  );
}

const AxisY = (i: number) => {
  return (
    <line
      key={i + 5000}
      x1={i}
      y1={0}
      x2={i}
      y2={500}
      strokeWidth={0.05}
      style={{
        stroke: 'grey',
        fill: 'none',
      }}
    />
  );
};

function AxisXText(i: number) {
  var inc: number;

  if (i < 9) {
    inc = 0.4;
  } else {
    inc = 0.7;
  }
  return (
    <text
      style={{ visibility: !i ? 'hidden' : 'visible' }}
      key={i + 2000}
      textAnchor="start"
      x={i - inc}
      y={0.6}
      fontSize={0.6}
    >
      {i}
    </text>
  );
}

const AxisYText = (i: number) => {
  var inc: number;

  if (i > 9) {
    inc = 0.4;
  } else {
    inc = 0.6;
  }
  return (
    <text key={i + 2000} textAnchor="start" y={i} x={0.1} fontSize={0.6}>
      {i}
    </text>
  );
};

// 👇️ named exports
export { AxisXText, AxisYText, AxisX, AxisY };
