import { ComponentType } from 'react';

import { AxisX, AxisXText, AxisY, AxisYText } from '../00grid/AxisSVG';
import styles from '../../../styles/Study.module.scss';

const GridCase: ComponentType = (): JSX.Element => {
  const pixelW: number = 200;
  const pixelH: number = 15;

  const pixelWVB: number = 20;
  const pixelHVB: number = 20;
  const VB: string = '0 0 ' + pixelWVB + ' ' + pixelHVB;

  return (
    <div className={styles.containerSVGParent}>
      <svg
        className={styles.containerSVG}
        viewBox={VB}
        preserveAspectRatio="xMaxYMin meet"
        style={{
          width: pixelW,
          height: pixelH,
        }}
      >
        {[...Array(pixelW)].map((x, j) => AxisX(j))}
        {[...Array(pixelH)].map((x, j) => AxisY(j))}
        {[...Array(pixelW)].map((x, j) => AxisXText(j))}
        {[...Array(pixelH)].map((x, j) => AxisYText(j))}

        <rect
          x="1"
          y="1"
          width="15"
          height="15"
          strokeWidth={0.1}
          style={{ stroke: '#ffffff', fill: 'none' }}
        />

        <rect
          x="0"
          y="0"
          width="15"
          height="15"
          strokeWidth={0.1}
          style={{ stroke: '#ffffff', fill: 'none' }}
        />
      </svg>
    </div>
  );
};

export default GridCase;
