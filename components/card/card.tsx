import Link from 'next/link';
import { ComponentType } from 'react';
import Image from 'next/image';
import styles from './card.module.scss';
 
const Card: ComponentType<CardType> = (props: CardType): JSX.Element => {
  return (
    <Link href={props.href} className={styles.LinkCase}>
      <a className={styles.cardLink}>
        <div className={`glass ${styles.container}`}>
          <div className={styles.cardHeaderWrapper}>
            <h2 className={styles.cardHeader}>{props.name}</h2>
          </div>
          <div className={styles.cardImageWrapper}>
            <Image
              alt={props.name}
              className={styles.cardImage}
              src={props.imgUrl}
              width={260}
              height={160}
            />
          </div>
        </div>
      </a>
    </Link>
  );
};

export default Card;
