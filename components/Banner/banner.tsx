import { ComponentType } from 'react';
import styles from './banner.module.scss';
const Banner: ComponentType<MyPropsButton> = (
  props: MyPropsButton
): JSX.Element => {
  return (
    <>
      <div className={styles.container} id="test">
        <h1 className={styles.title}>
          <span className={styles.title1}>Meta </span>
          <span className={styles.title2}>Coffee</span>
        </h1>
        <p className={styles.subTitle}>Discover your local coffee shops!</p>
        <div className={styles.buttonWrapper}>
          <button className={styles.button} onClick={props.handleOnClick}>
            {props.buttonText}
          </button>
        </div>
      </div>
    </>
  );
};

export default Banner;
