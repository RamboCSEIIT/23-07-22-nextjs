import styles from './Hero.module.scss';

import { ComponentType } from 'react';
const Hero: ComponentType<MyPropsButton> = (
  props: MyPropsButton
): JSX.Element => {
  return (
    <div className={styles.conatainer}>
      <h1 className={styles.title}>
        <span className={styles.title1}>
          {process.env.NEXT_PUBLIC_SITE_NAME}
        </span>
        <span className={styles.title2}>
          {process.env.NEXT_PUBLIC_TAG_NAME}
        </span>
      </h1>
      <p className={styles.subTitle}>Discover Your Favourite Coffee Shop</p>
      <div className={styles.buttonWrapper}>
        <button className={styles.button} onClick={props.handleOnClick}>
          {props.buttonText}
        </button>
      </div>
      {/* 
          
  */}
    </div>
  );
};

export default Hero;
