import { createApi } from "unsplash-js";

const unsplashApi = createApi({
  accessKey: `${process.env.NEXT_PUBLIC_UNSPLASH_ACCESS_KEY}`,
});

const options: any = {
  method: "GET",
  headers: {
    accept: "application/json",
    Authorization: process.env.NEXT_PUBLIC_FOURSQUARE_API_KEY,
  },
};
const getPlacesUrl = (query: any, latLong: any, limit: any) => {
  return `https://api.foursquare.com/v3/places/search?query=${query}&ll=${latLong}&limit=${limit}`;
};

export const fetchCoffeeStores = async (
  latLong = "28.750601,77.055504",
  limit = 10
) => {
  const response = await fetch(
    getPlacesUrl("coffee shop", latLong, limit),
    options
  );
  const data = await response.json();

  const photos: any = await GetListofCoffeeStorePhotos();

  return data.results.map((pic: any, idx: number) => {
    return {
      ...pic,
      imgUrl: photos[idx],
    };
  });
};

const GetListofCoffeeStorePhotos = async () => {
  const photos = await unsplashApi.search.getPhotos({
    query: "coffee shop",
    page: 1,
    perPage: 10,

    orientation: "portrait",
  });

  const Myresult = photos.response?.results;

  //console.log("key" + `${process.env.NEXT_PUBLIC_UNSPLASH_ACCESS_KEY}`);
  //console.log("%j", photos.response?.results);

  const Dummy = Myresult?.map((result) => result.urls["small"]);
  //console.log("%j", Dummy);
  console.log(JSON.stringify(Dummy));
  return Dummy;
};
