type ButtonPressFunction = () => void;
type MyPropsButton = {
  buttonText: string;
  handleOnClick: ButtonPressFunction;
};
type MyPropsButton1 = {
  buttonText: number;
};

type CardType = {
  name: string;
  href: string;
  imgUrl: imgUrl;
};
type CoffeeStoreType = {
  id: number;
  name: string;
  imgUrl: string;
  websiteUrl: string;
  address: string;
  neighbourhood: string;
};

type CoffeeStoresType = {
  fsq_id: number;
  name: string;
  imgUrl: string;
  websiteUrl: string;
  address: string;
  related_places: any;
  location: any;
};
type CoffeeStoreTypeDyanamicParam = {
  id: string;
};
